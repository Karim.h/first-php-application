<?php

require __DIR__ . '/../app/vendor/autoload.php';
function update($id, $label)
{
    if (empty($label)) {
        throw new Exception('Nom du produit vide');
    }
    $pdo = databaseConnexion();

    $stmt = $pdo->prepare('UPDATE product SET label = (:label) where ID = (:id)');
    $stmt->bindParam(':label', $label);
    $stmt->bindParam(':id', $id);

    $stmt->execute();
}

function getProduct($id) {
    $pdo = databaseConnexion();

    $sql = 'SELECT * FROM product where id = ' . $id;

    $stmt = $pdo->query($sql);
    $user = $stmt->fetch();

    return $user;
};

function databaseConnexion() {
    $dsn = 'mysql:host=mysql;dbname=test;charset=utf8;port=3306';
    $pdo = new PDO($dsn, 'dev', 'dev');

    return $pdo;
}

if (isset($_GET['product_id'])) {
    $product = getProduct($_GET['product_id']);
} else {
    echo 'il faut un produit en parametre';
    exit;
}


if (isset($_POST['id']) && isset($_POST['label'])) {
    update($_POST['id'], $_POST['label']);
    echo 'bien joué';
    exit;
} elseif(!empty($_POST)) {
    echo 'error';
    exit;
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Docker </title>
    </head>
    <body>
        <h1>Bonjour <? echo $_GET['prenom']; ?></h1>


        <form method="post">
            <p>
                <label>Ajouter un produit</label> : <input type="text" name="label" value="<? echo $product['label'] ?>"/>
                <input type="hidden" name="id" value="<? echo $product['id'] ?>">
            </p>

            <input type="submit" value="Envoyer" />
        </form>

    </body>
</html>
