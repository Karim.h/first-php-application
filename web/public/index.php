<?php

require __DIR__ . '/../app/vendor/autoload.php';
/*
try {

    $dsn = 'mysql:host=mysql;dbname=test;charset=utf8;port=3306';
    $pdo = new PDO($dsn, 'dev', 'dev');
    foreach($pdo->query("SELECT * from product") as $row) {
        var_dump($row);
    }
} catch (PDOException $e) {
    echo $e->getMessage();
}
*/
function test($productLabel)
{
    if (empty($productLabel)) {
        throw new Exception('Nom du produit vide');
    }
    $pdo = databaseConnexion();
    $stmt = $pdo->prepare("INSERT INTO product (label) VALUES (:label)");
    $stmt->bindParam(':label', $productLabel);

    $stmt->execute();
}

function databaseConnexion() {
    $dsn = 'mysql:host=mysql;dbname=test;charset=utf8;port=3306';
    $pdo = new PDO($dsn, 'dev', 'dev');

    return $pdo;
}

if (isset($_POST['label'])) {
    test($_POST['label']);
    echo sprintf('Produit envoyé via le formulaire : %s', $_POST['label']);
} else {
    echo 'aucun produit envoyé';
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Docker </title>
    </head>
    <body>
        <h1>Bonjour <? echo $_GET['prenom']; ?></h1>

    <? try {
        if (isset($_GET['prenom'])) {
            test($_GET['prenom']);
        }
    } catch(Exception $e) {
        echo $e->getMessage();
        exit;
    } ?>


        <form method="post">
            <p>
                <label>Ajouter un produit</label> : <input type="text" name="label" />
            </p>

            <input type="submit" value="Envoyer" />
        </form>

    </body>
</html>
